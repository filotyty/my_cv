class Cv {
    get companyName() {
        return this._companyName;
    }

    set companyName(value) {
        this._companyName = value;
    }

    get email() {
        return this._email;
    }

    set email(value) {
        this._email = value;
    }

    get date() {
        return this._date;
    }

    set date(value) {
        this._date = value;
    }

    get state() {
        return this._state;
    }

    set state(value) {
        this._state = value;
    }

    get job() {
        return this._job;
    }

    set job(value) {
        this._job = value;
    }

    get address() {
        return this._address;
    }

    set address(value) {
        this._address = value;
    }

    get cp() {
        return this._cp;
    }

    set cp(value) {
        this._cp = value;
    }

    get site() {
        return this._site;
    }

    set site(value) {
        this._site = value;
    }

    get idAdvertissement() {
        return this._idAdvertissement;
    }

    set idAdvertissement(value) {
        this._idAdvertissement = value;
    }

    constructor(companyName = "", email = "", date = new Date()) {
        this._companyName   = companyName;
        this._email         = email
        this._date          = date
        this._state         = null;
        this._job           = "";
        this._address       = "";
        this._cp            = "";
        this._site          = "";
        this._idAdvertissement = -1;
        //to complete
    }
}