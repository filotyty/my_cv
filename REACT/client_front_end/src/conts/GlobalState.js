import React, {createContext, useContext, useReducer, useState} from "react";
import AppReducer from './AppReducer';
import store from "store";
//Etat initial
const initialState = {
    _announcesList: [
        {
            _companyName: "1998",
            _email: "duramana.kalumvuati@laposte.net",
            _site: "https://www.domain.extension",
            _date: "2020-02-01",
            _cp: 65755,
            _address: "17 Route de toute direction",
            _idAdvertissement: 7854,
            _state: false,
            _job: "DEveloppeur Web",
            _answered: false,
        },
        {
            _companyName: "Ecole ",
            _email: "duramana.kalumvuati@laposte.net",
            _site: "https://www.domain.extension",
            _cp: 65755,
            _address: "17 Route de toute direction",
            _idAdvertissement: 7854,
            _date: "2020-07-10",
            _state: true,
            _job: "fgsdfg",
            _answered: true,
        },
        {
            _companyName: "cfii",
            _email: "filotyty@gmail.com",
            _site: "https://www.domain.extension",
            _cp: 65755,
            _address: "17 Route de toute direction",
            _idAdvertissement: 7854,
            _date: "2020-03-09",
            _state: false,
            _job: "fgsdfg",
            _answered: false,
        },
        {
            _companyName: "Centre de ",
            _email: "duramana.kalumvuati@laposte.net",
            _site: "https://www.domain.extension",
            _cp: 65755,
            _address: "17 Route de toute direction",
            _idAdvertissement: 7854,
            _date: "2020-07-2",
            _state: true,
            _job: "fgsdfg",
            _answered: true,
        },
        {
            _companyName: "CFII",
            _email: "filotyty@gmail.com",
            _site: "https://www.domain.extension",
            _cp: 65755,
            _address: "17 Route de toute direction",
            _idAdvertissement: 7854,
            _date: "2020-07-15",
            _state: false,
            _job: "Administateur Réseau",
            _answered: true,
        },{
            _companyName: "CFII",
            _email: "filotyty@gmail.com",
            _site: "https://www.domain.extension",
            _cp: 65755,
            _address: "17 Route de toute direction",
            _idAdvertissement: 7854,
            _date: "2020-07-15",
            _state: false,
            _job: "Administateur Réseau",
            _answered: true,
        },
    ],
    _name: "Duramana KALUMVUATI",
    _lastName: "Duramana",
    _email: "admin@t3t.fr",
    _password:'admin',
    _site: "https://www.domain.extension",
    _cv: "",
    _lm: "",
    _job: "Administrateur systeme",
    _answered: true,
    _isLoggedIn:store.get("loggedIn")
};


//1. elle attend une valeur initiale qu'on lui donne
//sur la ligne value= {{transaction:state.transactions }}
export  const GlobalContext = createContext(initialState);

export  const GlobalProvider = ({children})=> {

    //elle renvoi initialState vu que AppReducer fait un switch(action.type)
    const [state, dispatch] = useReducer(AppReducer, initialState);
    const {_announcesList,_name,_lastName,_email,_password,_site,_cv,_lm,_job,_answered,_isLoggedIn} = state;
    const announces = {_announcesList,_name,_lastName,_email,_password,_site,_cv,_lm,_job,_answered,_isLoggedIn};
    const [isLoggedIn, setisLoggedIn] = useState(announces._isLoggedIn);
    const {user}        = useContext(GlobalContext);

    const onDelete = function(id) {
        dispatch({
            type:'DELETE_ANNOUNCE',
            payload:id
        })
    }

    const onAdd = function(announce) {
        dispatch({
            type:'ADD_ANNOUNCE',
            payload: announce
        })
    }

    const onLogOut = ()=>{
        setisLoggedIn(false);
    }
    const onLogIn = (username,password)=>{
        const user = GlobalContext._currentValue;
        return (username === user._email &&
            password === user._password)
    }
    return (
        <GlobalContext.Provider value ={{
            user: announces,
            onDelete : onDelete,
            onAdd : onAdd,
            onLogOut:onLogOut,
            onLogIn:onLogIn
        }} >
            {children}
        </GlobalContext.Provider>
    );
}
