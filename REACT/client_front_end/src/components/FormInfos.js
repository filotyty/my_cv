import React, { Component } from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import RaisedButton from 'material-ui/RaisedButton'
import {Grid,TextField} from "@material-ui/core";
import NavBar from "../navbar/NavBar";

export class FormInfos extends Component {

    constructor(props) {
        super(props);
        this.continue = this.continue.bind(this);
    }

    continue = e=>{
        e.preventDefault();
        this.props.nextStep()
    }

    render() {
        const {values} = this.props
        return (<div>
            <MuiThemeProvider>
                <Grid
                    container
                    direction   = "column"
                    alignItems  = "center"
                    justify     = "center"
                    spacing     = {0}
                    style       = {{marginTop:"70px"}}
                >
                    <NavBar/>

                    <div
                        style = {{
                            fontSize:'1.2rem',
                            color:'#3f51b5',
                            fontWeight:"bold"
                        }}
                    >INFORMATIONS SUR L'ANNONCE</div>
                    <TextField
                        style               = {styles.textfield}
                        label               = 'Enter the advertisement number'
                        floatingLabelFixed  = {false}
                        type                = "number"
                        onChange            = {this.props.onChanged("_idAdvertissement")}
                        defaultValue        = {values._idAdvertissement === 0 ? '':values._idAdvertissement }
                    />
                    <br/>
                    <TextField
                        style           = {styles.textfield}
                        label           = "Enter the name of Company"
                        onChange        = {this.props.onChanged("_companyName")}
                        defaultValue    = {values._companyName}
                    />
                    <br/>

                    <TextField
                        style               = {styles.textfield}
                        label               = "Job's Post"
                        floatingLabelFixed  = {false}
                        onChange            = {this.props.onChanged("_job")}
                        defaultValue        = {values._job}
                    />
                    <br/>
                    <RaisedButton
                        label       = "Continue"
                        primary     = {true}
                        style       = {styles.button}
                        onClick     = {this.continue}
                    />
                </Grid>
        </MuiThemeProvider>
        </div>);
    }
}

const styles = {
    button: {
        margin:20
    },
    textfield: {
        width:'60vh'
    }
}
export default FormInfos;
