class User {

    constructor(name = null,lastName = null,email= null) {
        this._name      = name;
        this._lastName  = lastName;
        this._email     = email;
        this._cv        = "";
        this._lm        = "";
        this._announcesList  = [];
    }

    get name() {return this._name;}

    get lastname() { return this._lastName;}
    
    get email() {return this._email;}

    get value() {return this.toString().split(",")}

    get announces() {
        return this._announcesList;
    }

    set announces(value) {
        this._announcesList = value;
    }
    set name(value) {
        this._name = value;
    }

    set lastName(value) {
        this._lastName = value;
    }

    set email(value) {
        this._email = value;
    }

    set cv(value) {
        this._cv = value;
    }

    set lm(value) {
        this._lm = value;
    }

    static deleteAnnounce(idAnnounce) {
        const user = User.userFromLocalStore('user');
        user.announces.splice(idAnnounce,1);
        Store.updateUser(user);

        Store.app_home(user)

    }

    static userFromLocalStore(name) {
        let user    = Store.getUser(name);
        var newUser = null;
        if (user !== null) {
            newUser = new User(user._name,user._lastName,user._email);
            //user._announcesList = user._announcesList;
            let announces = user._announcesList;
            //traitement de dates
            announces.map((announce)=> {
                announce._date = new Date(announce._date);
                return announce;
            })
            //TODO - il faut les annonces aient comme instance de la classe CV ou Announce
            newUser._announcesList = announces
            newUser._lm = user._lm;
            newUser._cv = user._cv;
            //Ajout de prototype de la fonction delete : qui supprimera une annonce
            newUser.__proto__.delete = function (id) {
                if (id >= 0 && id <this._announcesList.length) {
                    console.log("Announce supprimé")
                    this._announcesList.splice(id,1);
                    Store.updateUser(newUser);
                    //Enleve l'announce de DOM
                    $('.announce_' + id).remove();
                }else {
                    console.log("Element n'existe pas")
                }
            }
        }

        return newUser;
    }


    static signUp() {
        //<!--     SIGNUP       -->
        Form.form('.app_body','',"app_signup");
        
        let userT = Store.split(new User(), '{}_"');

        //Affichage de champs
        userT.forEach((field) => {
            if (field.length !== 0) {
            let temp    = field.split(":");
            let temps2  = temp[0].toLowerCase();
            if (temp[1] !== "[]" && (temps2 === "cv" || temps2 === "lm"))
                Form.input(".app_signup", temps2, [{ type: "file" }]);
            else if (temp[1] !== "[]")
                Form.input(".app_signup", temps2, [{ type: "text" }]);
            }
        });
        Form.button(".app_signup", "Add", "btn btn-primary", "submit");
        $(".app_signup").addEventListener("submit", saveFields);

    }

    toString() {
        return "Name:" + this._name + ",Lastname:" + this._lastName + ",Email:" + this._email;
    }
}
