import React from "react";
import {Typography} from "@material-ui/core";

export  default function TitleH({children}) {


    return (
        <Typography
            variant='h4'
            component="h4"
            align="center"
            color="primary"
            style={{
                marginTop:'40px',
                marginBottom:'40px',
            }}

        >{children}</Typography>
    );
}