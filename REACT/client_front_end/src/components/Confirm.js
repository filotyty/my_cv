import React, { Component } from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import {List, ListItem} from 'material-ui/List'
import RaisedButton from 'material-ui/RaisedButton'
import NavBar from "../navbar/NavBar";
import {Grid} from "@material-ui/core";

export class Confirm extends Component {
    // constructor(props) {
    //     super(props);
    // }

    back = e=>{
        e.preventDefault();
        this.props.prevStep()
    }

    confirme = e=>{
        e.preventDefault();
        this.props.onConfirme()
        this.props.nextStep()
    }

    render() {
        const {values} = this.props

        return (
            <MuiThemeProvider >
                <React.Fragment>
                    <NavBar/>

                    <div style={styles.margins}>

                        <div
                            style   = {{
                                textAlign:'center',
                                fontSize:'1.2rem',
                                color:'#3f51b5',
                                fontWeight:"bold"
                            }}
                        >VEFICATIONS D'ANNONCE</div>

                        <List >
                            <ListItem
                                primaryText="Numéro d'annonce"
                                secondaryText={values._idAdvertissement}
                            />

                            <ListItem
                                primaryText="Entreprise"
                                secondaryText={values._companyName}
                            />
                            <ListItem
                                primaryText     = "Poste proposé"
                                secondaryText   = {values._job}
                            />
                            <ListItem
                                primaryText     = "Email"
                                secondaryText   = {values._email}
                            />
                            <ListItem
                                primaryText="City"
                                secondaryText={values._site}
                            />
                            <ListItem
                                primaryText     = "Numéro de téléphone"
                                secondaryText   = {values._telephone}
                            />
                            <ListItem
                                primaryText="Date d'envoi"
                                secondaryText={values._date}
                            />
                            <ListItem
                                primaryText     = "Code Postal"
                                secondaryText   = {values._cp}
                            />
                            <ListItem
                                primaryText="Adresse"
                                secondaryText={values._address}
                            />
                            <ListItem
                                primaryText     = "Ville"
                                secondaryText   = {values._city}
                            />
                            <ListItem
                                primaryText     = "Ont-ils Répondu ? "
                                secondaryText   = {values._answered === true? "Oui":"Non"}
                            />
                            {values._answeredStatus &&

                                <ListItem
                                    primaryText     = "Réponse? "
                                    secondaryText   = {values._answered === true? "Oui":"Non"}
                                />
                            }

                            <ListItem
                                primaryText     = "Question importante à poser"
                                secondaryText   = {values._usefulInfo}
                            />
                            {/*<List>*/}
                            {/*    { values._questions.forEach((info,index)=>*/}
                            {/*        <ListItem*/}
                            {/*            primaryText     = " Question N°  ${index}"*/}
                            {/*            secondaryText   = {info}*/}
                            {/*        />*/}

                            {/*    ) }*/}

                            {/*</List>*/}
                            <ListItem
                                primaryText     = "Date d'interview"
                                secondaryText   = {JSON.stringify(values._interviewDate)}
                            />
                        </List>

                        <RaisedButton label="Back"
                                      primary={false}
                                      style={styles.button}
                                      onClick={this.props.prevStep}/>

                        <RaisedButton label="Confirme"
                                      primary={true}
                                      style={styles.button}
                                      onClick={this.confirme}/>
                    </div>

                </React.Fragment>

            </MuiThemeProvider>);
    }
}

const styles = {
    button: {
        margin:20
    },
    margins: {
        margin:"70px auto"
    }
}
export default Confirm;
