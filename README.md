## MY CV STORE


**Objetif :** 

ça permet à un utilisateur de sauvergarder les annonces des propositions d'embauche qu'il a postilées.

Ex: sur l'IHM se trouve dans le repertoire nommé : 'drawable-hdpi'.


Origine : 

Lors de mes recherches de stages ou emploi. Après avoir postilé à une offre quelconque, lors que les réponses arrivaient sur ma boîte mail, je n'arrivais plus à savoir laquelle des entreprises/annonces postilée m'a répondue.

En plus de ça, en continuant mes recherches d'emploi, ça m'arrivait d'envoyer de nouveau mon CV à l'entreprise qui m'a déjà répondu.

Alors, j'avais l'obligation d'avoir un support me permettant de noter les annonces ou entreprises que j'ai déjà envoyées un CV.


Voilà pour quoi, j'ai eu l'idée de mettre en place cette application pour m'aider à resoudre ce petit problème.



## MODE UTILISATION

Au debut, l'application vérifie sur la mémoire 'Local Storage' de navegateur pour trouver l'indice nommé 'user' :

1. S'il existe, elle va parcourir les annonces que possède ce dernier pour les afficher toutes.

2. Sinon, elle va proposer à l'utilisateur de faire le 'SIGN UP', pour pouvoir commencer à utiliser l'aaplication.

NB : j'ai l'intension de mettre en place une base de données commune permettant à y sauvegarder les informations de l'utilisateur. Mais comme j'utilise pour l'instant 'Javascript, CSS, JSON et HTML', ceux-ci me suffisent largement.

Mais, il y a le framework Adonisjs, nodejs, React qui pouront tout simplement nous aider à réaliser la partie base de données sans problème.


##  DEVELOPPEMENT

Pour l'instant, j'ai mise en plcae un fichier sur la racine nommé 'app.js' qui permet de tout gerer.

Pour lancer l'application, il suffit avoir sur l'éditeur :
* Visual Studio Code
* un package nommé : 'Live Server'
* aller sur le repertoire 'public' où se trouve le fichier 'index.html', pour lancer le serveur en faisant : clique droit + 'Open with a Live Server', ou soit ALT + L ALT + O
* pour être précis, voici l'url : http://127.0.0.1:5500/public/


## IHM : 

sur le repertoire : 

 * drawable-hdpi

## GESTIONAIRE DE VERSION : 

git, git flow


**REACT**

On trouve sur le repertoire nommé "my_cv/REACT/client_front_end" la version React du projet partie Front-End.

**LANCEMENT DU PROJET**

Une fois dans ce même repertoire, suivez les instructions ci-dessous.

Pré-installation : sur votre machine, vous devez avoir ces applications ou framework afin de continuer.

1. nodejs
2. npm ou yarn
3. etc ;-)

Après les avoir toutes mises en place faites ces commandes pour lancer le projet :

1. npm/yarn install
2. yarn start

Normalement, vous devez voir sur votre écran un lancement automatique de navegateur(vous devez en avoir un installé, bie -sûr).

NB : j'ai bouchoné l'affichage dans cette stade pour me permettre à resoudre un petit problème. Et pour éviter chaque fois d'introduire les données
j'en ai mises une quantité suffisante pour avancer sans problème.

Enjoy!

