import React, {Component, useState} from "react";
import {Grid} from "@material-ui/core";
import {Button,TextField} from "@material-ui/core";
import {GlobalContext} from "../conts/GlobalState";
import store from 'store';
import Alert from '@material-ui/lab/Alert';
import NavBar from "../navbar/NavBar";



export default class Login extends Component{
    static contextType = GlobalContext;
    constructor(props) {
        super(props);
        this.state = {
            username:"",
            password:"",
            logState:false
        }
    }

    handlerChanged = (e)=>{
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit = (e)=> {
        const {onLogIn} = this.context;
        const {history }    = this.props;
        if (onLogIn(this.state.username,this.state.password))
        {
            store.set("loggedIn",true);
            history.push("/");
        }else {
            this.setState({
                error:true
            });
            store.set("loggedIn",false);
            history.push("/login");
        }

    }


    render() {
        return (
            <Grid
                container
                direction   = "column"
                alignItems  = "center"
                justify     = "center"
                spacing     = {0}
                style       = {{marginTop:"70px"}}
            >
            <NavBar/>
                <h3>LOG IN</h3>
                {this.state.error && <Alert severity="error" style={styles.root}>Veuillez vérifier vos coordonnées!</Alert>
                }
                <TextField
                    style   = {styles.textfield}
                    label   = "Username"
                    name    = "username"
                    value   = {this.state.username}
                    onChange= {this.handlerChanged}
                />

                <TextField
                    style   = {styles.textfield}
                    label   = "Password"
                    name    = "password"
                    value   = {this.state.password}
                    onChange= {this.handlerChanged}
                    type    = "password"
                />

                <Button
                    type    = "submit"
                    variant = "contained"
                    color   = "primary"
                    style   = {{marginTop:"20px"}}
                    onClick = {this.onSubmit}
                >Connect</Button>
            </Grid>);
    }



}

const styles = {
    button: {
        margin:20
    },
    textfield: {
        width:'60vh'
    },
    root: {
        width: '60%',
        '& > * + *': {
            marginTop: 2,
        },
    },
}