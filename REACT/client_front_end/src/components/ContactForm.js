import React, { Component } from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import RaisedButton from 'material-ui/RaisedButton'
import { Grid,TextField} from "@material-ui/core";
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import NavBar from "../navbar/NavBar";

export class ContactForm extends Component {

    constructor(props) {
        super(props);
        this.continue = this.continue.bind(this);
    }

    back = e=>{
        e.preventDefault();
        this.props.prevStep()
    }

    continue = e=>{
        e.preventDefault();
        this.props.nextStep()
    }

    render() {
        const {values} = this.props
        return (<div>
            <MuiThemeProvider>
                <Grid
                    container
                    direction   = "column"
                    alignItems  = "center"
                    justify     = "center"
                    spacing     = {0}
                    style       = {{marginTop:"70px"}}
                >
                    <NavBar/>

                    <div
                        style   = {{
                            fontSize:'1.2rem',
                            color:'#3f51b5',
                            fontWeight:"bold"
                        }}
                    >CONTACTS DE L'ENTREPRISE</div>
                    <ValidatorForm
                        ref="form"

                    >
                        <TextValidator
                            errorMessages       = "Email incorrect"
                            style               = {styles.textfield}
                            label               = "Tapez le mail de l'entreprise"
                            type                = "email"
                            validators          = {['isEmail']}
                            floatingLabelFixed  = {true}
                            onChange            = {this.props.onChanged("_email")}
                            value               = {values._email}/>

                        <TextField
                            type                = "text"
                            style               = {styles.textfield}
                            label               = "Enter your site"
                            floatingLabelFixed  = {true}
                            onChange            = {this.props.onChanged("_site")}
                            value               = {values._site}/>
                        <br/>
                        <TextField
                            style               = {styles.textfield}
                            label               = "Enter your phone number"
                            floatingLabelFixed  = {true}
                            type                = "tel"
                            onChange            = {this.props.onChanged("_telephone")}
                            value               = {values._telephone === 0 ? '':values._telephone}/>
                        <br/>
                        <TextField
                            style               = {{marginTop:'20px',width:'60vh' }}
                            floatingLabelFixed  = {true}
                            type                = "date"
                            onChange            = {this.props.onChanged("_date")}
                            value               = {values._date === 0 ? '':values._date}
                        />
                        <br/>
                    </ValidatorForm>
                    <Grid
                        container
                        direction   = "row"
                        alignItems  = "center"
                        justify     = "center"
                        spacing     = {0}
                        style       = {{marginTop:"70px"}}
                    >

                        <RaisedButton
                            label       = "Back"
                            primary     = {true}
                            style       = {styles.button}
                            onClick     = {this.back}
                        />

                        <RaisedButton
                            label       = "Continue"
                            primary     = {true}
                            style       = {styles.button}
                            onClick     = {this.continue}/>
                    </Grid>

                </Grid>
            </MuiThemeProvider>
        </div>);
    }
}

const styles = {
    button: {
        margin:20
    },
    textfield: {
        width:'60vh'
    }
}
export default ContactForm;
