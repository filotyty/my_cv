import React, { useEffect, useState,useContext} from "react";
import Input from '@material-ui/core/Input';
import {Container, Grid} from "@material-ui/core";
import Cards from "./Cards";
import {GlobalContext, GlobalProvider} from "../conts/GlobalState";
import NavBar from "../navbar/NavBar";


export default function Home() {
    const [search,setSearch]        = useState('');
    const {user}                    = useContext(GlobalContext)
    const [announcesT,setAnnounces] = useState(user._announcesList);

    //Filtre par les antités des annonces
    //TODO correction de  cet endroit
    useEffect((e)=>{
        if(search.length> 0) {
            setAnnounces(announcesT.filter(announce =>
                announce._companyName.toLowerCase().indexOf(search.toLowerCase())   !== -1 ||
                announce._site.toLowerCase().indexOf(search.toLowerCase())          !== -1 ||
                announce._email.toLowerCase().indexOf(search.toLowerCase())         !== -1 ||
                announce._date.toLowerCase().indexOf(search.toLowerCase())          !== -1
            ))
        }else{
            setAnnounces(user._announcesList);
        }
    },[search,announcesT,setAnnounces]);

    const onChange = (e)=> {
        setSearch(e.target.value);
    }

    return( <Container style = {{
            marginTop:"80px",
            marginBottom:"70px",
        }}>
        <NavBar/>

        <div
            style={{
                fontSize:'1.8rem',
                color:'#3f51b5',
                fontWeight:"bold",
                textAlign:'center',
                margin:'20px'
            }}
        >ANNOUNCES DEJA POSTILES</div>
        <Input
            name        = "search"
            placeholder = "Search…"
            inputProps  = {{ 'aria-label': 'description' }}
            style       = {{
                width:'100%',
                marginBottom:'10px'
            }}
            value       = {search}
            onChange    = {onChange}
        />

        <div>
            { announcesT.map((announce,index) => {
            return <Cards
                key     = {index}
                firmName= {announce._companyName}
                email   = {announce._email}
                date    = {announce._date}
            />
        })}
        </div>
        </Container>);
}


