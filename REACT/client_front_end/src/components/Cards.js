import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection:'row',
        marginBottom:"10px"
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
        width:"100%",
        flexWrap:"wrap",
        maxWidth:"70%"
    },
    cover: {
        width: '40%',
        cursor:'pointer'
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    }
}));

export default function Cards({firmName,email,date}) {
    const classes = useStyles();
    const onClick = (e)=> {
        console.log("Show this announce")
    }
    return (
        <Card className={classes.root}>
            <CardMedia
                onClick={onClick}
                className={classes.cover}
                image={process.env.PUBLIC_URL + "/images/1.png"}
                title="Live from space album cover"
            />

            <div className={classes.details}>
                <CardContent className={classes.content} >
                    <Typography component="h6" variant="h6" color="primary">
                        {firmName}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                        {email}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                        {date}
                    </Typography>
                </CardContent>

            </div>

        </Card>
    );
}

Cards.propTypes = {
    firmName: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
}
