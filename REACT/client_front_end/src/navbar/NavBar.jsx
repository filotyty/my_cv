import React, {useContext} from "react";
import  {AppBar, Toolbar, IconButton, Typography,Button,makeStyles} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import {GlobalContext} from "../conts/GlobalState";

const useStyles = makeStyles((theme) => ({
    root: {
        position: "fixed",
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    }
}));

export default function NavBar() {
    const {onLogOut}    = useContext(GlobalContext);
    const user          = useContext(GlobalContext);
    const classes = useStyles();
    const bntState = user._isLoggedIn !== true ? "LOG OUT":"LOG IN";
    return (<>
        <AppBar position="fixed">
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                    <MenuIcon />
                </IconButton>
                <Typography variant="h5" className={classes.title} align="center">
                    <Button
                        variant="text"
                        color="inherit"
                        href="/"
                    >HOME</Button>
                </Typography>

                <Button
                    variant="contained"
                    color="default"
                    name="login"
                    onClick={onLogOut}
                    href={ user._isLoggedIn === true? "/logout":"/login"}
                >
                    {bntState}
                </Button>:

            </Toolbar>
        </AppBar>
    </>);

}