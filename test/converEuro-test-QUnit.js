function testcase(message, tests){

    var total       = 0;
    var succeed     = 0;
    var hasSetup    = typeof tests.setUp === 'function';
    var hasTeardown = typeof tests.tearDown === 'function';
    var p           = document.createElement('p');
    p.innerHTML     = message;
    document.body.appendChild(p);
    
    for(test in tests){
     if(test !== 'setUp' && test !== 'tearDown'){
      total++;
     }
     try{
      if(hasSetup){
        tests.setUp();
      }
      tests[test]();
      if(test !== 'setUp' && test !== 'tearDown'){
       succeed++;
      }
      if(hasTeardown){
       tests.tearDown();
      }
     }catch(err){  
     }
    }
    var p = document.createElement('p');
    p.innerHTML = 'succeed tests ' + succeed + '/' + total ;
    document.body.appendChild(p);
}
   
    testcase('I convert euro to usd', {
    'setUp' : function(){
        this.currency = 'USD';
    },
    'I test with one euro' : function(){
        assert('1€ should return 1,3$', convertEuro(1, this.currency) == 1.3);
    },
    'I test with two euros' : function(){
        assert('2€ should return 2,6$', convertEuro(2, this.currency) == 2.6);  
    }
})