class Form {
    static  fileTypes = [
        'image/jpeg',
        'image/jpg',
        'image/png'
    ]
    constructor() {

    }
    /**
     *Elle mette en place un champ selon les données passées en paramètres
     * @param selector
     * @param name
     * @param options
     * @param value
     */
    static input(selector, name, options = Array(),value = ""){
        if (selector) {
            var content         = document.querySelector(selector);
            var inputElement    = document.createElement('input');
            options.forEach((option)=>{
                inputElement.type   = (option.type)?option.type:"text";
            })
            inputElement.id         = name.toLowerCase();
            inputElement.className  = "form-control";
            inputElement.name       = name;
            inputElement.value      = value;
            inputElement.style.marginTop    = "10px";
            inputElement.placeholder        = "set your " + name;
            content.appendChild(inputElement);
        }
    }

    /**
     * Elle mette en place un boutton selon les arguments passés en paramètres
     * @param selector
     * @param name
     * @param className
     * @param type
     * @param callback_function
     */
    static button(selector, name, className, type = "", callback_function = null){
        var content     = document.querySelector(selector);
        var btn         = document.createElement('button');
        btn.textContent = name;
        btn.id          = name.toLowerCase();
        btn.className   = className;
        if (type.length > 0)
            btn.type    = type;
        btn.name        = name.toLowerCase();
        btn.style.marginTop    = "10px";
        btn.placeholder        = "set your " + name;
        content.appendChild(btn);

        if (callback_function !== null)
            document.querySelector('#' + name.toLowerCase()).addEventListener('click',callback_function);
    }

    static form(selector = "", action = "", name = "") {
        var content             = document.querySelector(selector);
        var formElement         = document.createElement('form');
        formElement.className   = `form-group ${name.toLowerCase()}`;
        formElement.style.marginTop    = "10px";
        content.appendChild(formElement);
    }

    static validFileType(file) {
        for(let i = 0; i < this.fileTypes.length; i++) {
            if(file.type === this.fileTypes[i]) {
                return true;
            }
        }

        return false;
    }

    static buttonSpecial(btnclass ="bg-success",
                         app_cv_update = 'app_cv_update',
                         children = "",
                         icon = 'fa-thumbs-up') {
        return '<div >' +
                '<div class="text-center titre_nav align-middle ">\n' +
                ' <a class="py-2 d-none d-md-inline-block "'+btnclass+' href="#" id='+app_cv_update+'>\n' +
                    ' <span>children</span>\n' +
                    ' <i class="fa "'+icon+'></i>\n' +
                ' </a>\n' +
                ' </div>'+
            '</div>';
    }

    static updateFile(selectorElement) {
        var img = document.createElement("img");
        var input = document.querySelector(selectorElement);
        var curFiles = input.files;
        const df = new FileReader();
        var isRead = false;
        df.addEventListener('load',(e)=>{
            if (e.loaded && e.total) {
                console.log("is Loading")
                console.log(e.loaded)
                console.log(e.total)
                var path = (window.URL || window.webkitURL).createObjectURL(curFiles[0]);
                console.log(path);

            }
        })
        df.readAsDataURL(curFiles[0])
        console.log(df)

        for (var i = 0; i < curFiles.length; i++) {
            if (!Form.validFileType(curFiles[i])) {
                console.log("Type de fichier non autorisé");
            }
        }
    }
}