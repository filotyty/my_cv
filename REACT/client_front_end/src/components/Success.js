import React, { Component } from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Grid} from "@material-ui/core";
import NavBar from "../navbar/NavBar";

export class Success extends Component {

    constructor(props) {
        super(props);
        this.continue = this.continue.bind(this);
    }

    back = e=>{
        e.preventDefault();
        this.props.prevStep()
    }

    continue = e=>{
        e.preventDefault();
        this.props.nextStep()
    }

    render() {
        return (<div>
            <MuiThemeProvider>
                <Grid
                    container
                    direction   = "column"
                    alignItems  = "center"
                    justify     = "center"
                    spacing     = {0}
                    style       = {{marginTop:"70px"}}
                >
                    <NavBar/>

                    <div
                        style   = {{
                            textAlign:'center',
                            fontSize:'1.2rem',
                            color:'green',
                            fontWeight:"bold",
                            marginBottom:"20px"
                        }}
                    >FELICITATIONS!</div>
                    <img width="100%" src={process.env.PUBLIC_URL + "..//images/1.png"} alt="Félicitations"/>

                </Grid>
            </MuiThemeProvider>
        </div>);
    }
}

export default Success;
