import React, { Component } from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import { Grid} from "@material-ui/core";
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import NavBar from "../navbar/NavBar";

export class AddressForm extends Component {
    constructor(props) {
        super(props);
        this.continue = this.continue.bind(this);
    }
    back = e=>{
        e.preventDefault();
        this.props.prevStep()
    }
    continue = e=>{
        e.preventDefault();
        this.props.nextStep()
    }
    render() {
        const {values} = this.props
        return (<div>
            <MuiThemeProvider>
                <Grid
                    container
                    direction   = "column"
                    alignItems  = "center"
                    justify     = "center"
                    spacing     = {0}
                    style       = {{marginTop:"70px"}}
                >
                    <NavBar/>

                    <div
                        style={{
                            fontSize:'1.2rem',
                            color:'#3f51b5',
                            fontWeight:"bold"
                        }}
                    >ADRESSE</div>

                    <ValidatorForm
                    >
                        <TextValidator
                            label           = "Code Postal"
                            name            = "_zip"
                            style           = {styles.textfield}
                            onChange        = {this.props.onChanged("_cp")}
                            type            = "zip"
                            validators      = {['isNumber', 'required']}
                            errorMessages   = {['Numeric value']}
                            value           = {values._cp ===0? "": values._cp}
                        />
                        <TextValidator
                            label           = "Tapez votre Adresse complète"
                            onChange        = {this.props.onChanged("_address")}
                            type            = "text"
                            style           = {styles.textfield}
                            name            = "_address"
                            validators      = {['isAddress', 'required']}
                            errorMessages   = {['Adresse vide', 'this field is required']}
                            value           = {values._address}
                        />
                    </ValidatorForm>
                    <br/>
                    <Grid
                        container
                        direction   = "row"
                        alignItems  = "center"
                        justify     = "center"
                        spacing     = {0}
                        style       = {{marginTop:"70px"}}
                    >

                            <RaisedButton label ="Back"
                                  primary           ={true}
                                  style             ={styles.button}
                                  onClick           ={this.back}/>

                            <RaisedButton label ="Continue"
                                  primary           ={true}
                                  style             ={styles.button}
                                  onClick           ={this.continue}/>
                    </Grid>

                </Grid>
            </MuiThemeProvider>
        </div>);
    }
}
const styles = {
    button: {
        margin:20
    },
    textfield: {
        width:'60vh'
    }
}
export default AddressForm;
