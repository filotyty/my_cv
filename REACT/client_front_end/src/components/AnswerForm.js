import React, {Component} from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import {Grid,Checkbox} from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import NavBar from "../navbar/NavBar";

export class AnswerForm extends Component {
    constructor(props) {
        super(props);
        this.continue = this.continue.bind(this);
    }
    back = e=>{
        e.preventDefault();
        this.props.prevStep()
    }
    continue = e=>{
        e.preventDefault();
        this.props.nextStep()
    }


    render() {
        const {values} = this.props
        return (<div>
            <MuiThemeProvider>
                <Grid
                    container
                    direction   = "column"
                    alignItems  = "center"
                    justify     = "center"
                    spacing     = {0}
                    style       = {{marginTop:"70px"}}
                >
                    <NavBar/>

                    <div
                        style={{
                            fontSize:'1.2rem',
                            color:'#3f51b5',
                            fontWeight:"bold"
                        }}
                    >RETOUR DE L'ENTREPRISE </div>

                    <FormControlLabel
                        control={
                            <Checkbox
                                label = "Checbox na ngai"
                                checked             = {values._answered}
                                inputProps          = {{'aria-label':'secondary checkbox'}}
                                onChange            = {this.props.onChecked("_answered")}
                                defaultChecked      = {false}
                            />
                        }
                        label="Avez-vous une réponse de l'entreprise?"
                    />

                    {values._answered &&  <FormControlLabel
                        control={
                            <Checkbox
                                label = "Checbox na ngai"
                                checked             = {values._answeredStatus}
                                inputProps          = {{'aria-label':'secondary checkbox'}}
                                onChange            = {this.props.onChecked("_answeredStatus")}
                                defaultChecked      = {false}
                            />
                        }
                        label="Cochez si c'est positive?"
                    />}
                    <br/>


                    <Grid
                        container
                        direction   = "row"
                        alignItems  = "center"
                        justify     = "center"
                        spacing     = {0}
                        style       = {{marginTop:"70px"}}
                    >

                        <RaisedButton label ="Back"
                                      primary           ={true}
                                      style             ={styles.button}
                                      onClick           ={this.back}/>

                        <RaisedButton label ="Continue"
                                      primary           ={true}
                                      style             ={styles.button}
                                      onClick           ={this.continue}/>
                    </Grid>

                </Grid>
            </MuiThemeProvider>
        </div>);
    }
}

const styles = {
    button: {
        margin:20,
    }
}
export default AnswerForm;
