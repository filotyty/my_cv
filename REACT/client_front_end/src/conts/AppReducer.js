export default  (state,action)=> {
    switch (action.type) {
        case 'DELETE_ANNOUNCE':
            return {
                ...state,
                _announcesList: state._announcesList.filter(announce => announce._idAdvertissement !== action.payload)
            }
        case 'ADD_ANNOUNCE':
            return {
                ...state,
                _announcesList: [action.payload, ...state._announcesList]
            }
        default:
            return state
    }
}