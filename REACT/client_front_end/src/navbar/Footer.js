import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import EventBusyIcon from '@material-ui/icons/EventBusy';
import ReceiptIcon from '@material-ui/icons/Receipt';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';

const useStyles = makeStyles((theme) => ({
    text: {
        padding: theme.spacing(2, 2, 0),
    },
    paper: {
        paddingBottom: 50,
    },
    list: {
        marginBottom: theme.spacing(2),
    },
    subheader: {
        backgroundColor: theme.palette.background.paper,
    },
    appBar: {
        top: 'auto',
        bottom: 0,
    },
    grow: {
        flexGrow: 1,
    },
    fabButton: {
        position: 'absolute',
        zIndex: 1,
        top: -30,
        left: 0,
        right: 0,
        margin: '0 auto'
    },
    paddind: {
        border:'1px solid #fff',
    }
}));

export default function Footer() {
    const classes = useStyles();
    const onAdd = (e)=>{
        console.log("Additionner une announce")
    }
    const onClick =(e)=>{
        if (e.target.tagName === 'svg')
        {
            console.log(e.target)
        }
    }
    return (
        <React.Fragment>
            <AppBar position="fixed" color="primary" className={classes.appBar}>
                <Toolbar>

                    <Fab color="secondary"
                         aria-label="add"
                         className={classes.fabButton}
                         onClick={onAdd}
                         href="/add"

                    >
                        <AddIcon />
                    </Fab>

                    <IconButton color="inherit" onClick={onClick} className={classes.paddind}>
                        <EventAvailableIcon id="update" />
                    </IconButton>

                    <div className={classes.grow} />
                    <IconButton color="inherit" onClick={onClick} className={classes.paddind}>
                        <EventBusyIcon id="deprecad" />
                    </IconButton>

                    <div className={classes.grow} />
                    <IconButton color="inherit" onClick={onClick} className={classes.paddind}>
                        <ReceiptIcon id="answers"/>
                    </IconButton>

                    <div className={classes.grow} />
                    <IconButton color="inherit" onClick={onClick}  className={classes.paddind}>
                        <ThumbUpAltIcon id="apointement" />
                    </IconButton>

                </Toolbar>
            </AppBar>
        </React.Fragment>
    );
}
