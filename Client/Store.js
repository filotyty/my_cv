/**
 * elle permet de se reperer par rapport
 * au nombre de jours passé depuis l'envoi de CV
 * @type {number}
 * @private
 */
const _DAYS_NUMBER = 14;

class Store {
  /**
   * @bref Elle ajoute un élément sur la mémoire du navegateur
   * @param newCV
   */
  static setCv(announce, id = -1) {
    let user = User.userFromLocalStore('user')
    if (id === -1) user._announcesList.push(announce);
    else user._announcesList[id] = announce;
    this.updateUser(user);
  }

  /**
   * Elle renvoit les éléments se trouvant sur la mémoire
   * du navegateur sous forme d'un tableau
   * @returns {[]}
   */
  static getCv() {
    let user = User.userFromLocalStore('user')
    return user._announcesList;
  }

  static app_get_cv_by(id) {
    let user = User.userFromLocalStore('user')

    return user._announcesList[id];
  }

  static get2rang(value) {
    return value < 10 ? '0'+value:value;
  }
  /**
   * Cette fonction est utilisée pour l'édition de l'addition d'une annonce
   * Elle affiche les champs pour pour qu'un utilisateur de cadestre.
   * Elle ajoute un événement pour le boutton Save
   */
  static app_add(announce = new Cv(), id = -1) {

    var cv              = announce;
    var btn_text        = "Save";
    var cancelOrdelete  = "Annuler";
    var title           = "Add a new annouce";
    var callback        = (id)=>Store.home(id);
    //remove home
    Store.setEmpty(".app_body");

    Form.form(".app_body","","add_announce","");

    //Edition
    if (cv !== null && cv._idAdvertissement >= 0) {
      title           = "Edit annouce N° : " + id;
      btn_text        = "Edit";
      cancelOrdelete  = "Delete";
      callback        = (id)=>User.deleteAnnounce(id)
      Form.input(".app_body", "id", [""], id);
      $("#id").disabled = true;
    }
    
    //traitement de la date
    let date = cv._date.getFullYear()+'-' +
        Store.get2rang(cv._date.getMonth()) + '-' +
        Store.get2rang(cv._date.getDate());

    Form.input(".app_body", "CompanyName", [], cv._companyName);
    Form.input(".app_body", "job", [{ placeholder: "Poste" }], cv._job);
    Form.input(".app_body", "address", [], cv._address);
    Form.input(".app_body", "email", [{ type: "email" }], cv._email);
    Form.input(".app_body", "cp", [{ type: "number" }], cv._cp);
    Form.input(".app_body", "date", [{ type: "date" }], date);
    Form.input(".app_body", "site", [], cv._site);
    Form.input(
      ".app_body",
      "idAdvertissement",
      [{ type: "number" }],
      cv._idAdvertissement === -1?"":cv._idAdvertissement
    );

    Form.button(
      ".app_body",
      btn_text,
      "btn btn-success",
      "submit",
      Store.app_cv
    );

    Form.button('.app_body',cancelOrdelete,
        'btn btn-danger','submit',()=>callback(id))

  }

  /**
   * Elle est appelée pour prendre en charge le traitement
   * des inputs, vérifications et sauvegarder l'annonce sur la mémoire
   * @param e
   */

  static app_cv(e) {

    e.preventDefault();
    const newAnnounce       = new Cv();
    newAnnounce.companyName = document.getElementById("companyname").value;
    newAnnounce.job         = document.getElementById("job").value;
    newAnnounce.address     = document.getElementById("address").value;
    newAnnounce.email       = document.getElementById("email").value;
    newAnnounce.cp          = document.getElementById("cp").value;
    newAnnounce.date        = document.getElementById("date").value;
    newAnnounce.site        = document.getElementById("site").value;
    newAnnounce.idAdvertissement= document.getElementById("idadvertissement").value;

    if (
      newAnnounce.companyName.length  === 0 ||
      newAnnounce.job.length          === 0 ||
      newAnnounce.address.length      === 0 ||
      newAnnounce.cp.length           === 0 ||
      newAnnounce.date.length         === 0 ||
      newAnnounce.site.length         === 0 ||
      newAnnounce.idAdvertissement.length === 0 
    ) {
      
      Store.alert(
        ".returnMessage",
        "alert alert-danger",
        "Tous les champs doivent être remplis",3000
      );
      
      newAnnounce.date = new Date(newAnnounce.date);
      
    } else {

      //TODO id = idAd=> creation d'une fonction qui vérifié si cette annonce existe déjà
      const id = document.getElementById("id");

      Store.setCv(newAnnounce, id !== null ? id.value : -1);

      //Retour vers la page home
      const user = User.userFromLocalStore('user');
      Store.alert(
        ".returnMessage",
        "alert alert-success",
        "Addition d'une nouvelle annonce avec succes!",3000
      );
      Store.app_home(user);
    }
  }

  static calculeDiffDate(oldDate, newDate = new  Date()) {
    let d1 = oldDate.getTime() / 86400000;
    let d2 = newDate.getTime() / 86400000;
    return new Number(d2 - d1).toFixed(0);
  }
  /**
   * Elle vérifie l'existance de user dans le localStore ~= DB
   * Elle est faite une fois seulement lors de lancement du
   * programme pour permettre acquisition des données de l'utilisateur
   * @returns {*}
   */
  static check_user(userName='user') {

    let user = null;
    if ((user = User.userFromLocalStore(userName)) === null) {

      //Préparation pour afficher les champs afin que l'utilisateur puisse y mettre ses informations
      //Affichage de champs pour obtenir les informations de user
      user = User.signUp();
      
    }
    return user;
  }

  /**
   * Enleve les caractères passés en parametre puis fait split du reste
   * @param user
   * @param caracteres
   * @returns {string[]}
   */
  static split(user = new Object(), caracteres = "") {
    var stringify = JSON.stringify(user);
    var index     = 0;

    for (var i = 0; i < caracteres.length; i++) {
      while (stringify.search(caracteres[i]) >= 0) {
        stringify = stringify.replace(caracteres[i], "");
      }
    }

    return stringify.split(",");
  }

  static getUser(key) {
    return JSON.parse(localStorage.getItem(key));
  }

  static cleanField(selectorName) {
    var inputs = document.querySelectorAll(selectorName);

    //clear inputs fields
    inputs.forEach((input) => {
      input.value = "";
    });
  }

  static show(selector) {
    $(selector).style.display = "block";
  }

  static hide(selector) {
    if ($(selector))
      $(selector).style.display = "none";
  }

  static remove(selector) {

    if ($(selector) !== null )
      $(selector).remove();
  }

  static setEmpty(selector ) {
    if ($(selector)) {
      $(selector).innerHTML = "";
    }
  }
  //TODO - fonction permettant de supprimer un CV
  static app_show(e) {
    e.preventDefault();
    var id    = e.target.id;
    let user  = User.userFromLocalStore('user');
    if (Number.isInteger(parseInt(id))) {
      /*
          TODO - Préparation d'une nouvelle fenetre pour affiche
          le CV selectionné avec tous les details possible
      */
      Store.app_add(Store.app_get_cv_by(id),id);
      Form.input('')
    } else {
      if (Number.isInteger(parseInt(e.target.parentElement.id))) {
          console.log("Erreur à gerer")

      } else {
        if (e.target.classList.contains("site"))
          console.log(
            "Ce lien de site internet de l'entreprise doit être traité"
          );
        else console.log("Mauvais endroit pour acceder à cet élément");
      }
    }

  }

  static home(useless=0) {
    console.log("Home")
    let user = User.userFromLocalStore('user');
    Store.app_home(user);
  }

  /**
   * cv_state :
   * @param cv_state
   */
  static app_home(user, cv_state = null) {
    /**
     * Nettoyer le app_body pour y mettre des nouvels éléments
     * @type {string}
     */
    $(".app_body").innerHTML = "";

    var app_home = document.createElement("div");
    app_home.className = "app_home ";
    app_home.style.display = "none";
    app_home.innerHTML =
      "" +
      '<form class="form-group " id="app_form" >\n' +
      ' <div class="app_input" id="app_input_form">\n' +
      "\n" +
      "</div>\n" +
      "</form>";
    //attacher app_home à app_body---------------
    //Component Parent où se trouvent d'autres components children
    $(".app_body").appendChild(app_home);


    var parentNod = $(".app_home");
    if (parentNod.style.display !== "none") return;
    //Efface le contenu pour y mettre de nouveau
    parentNod.innerHTML     = "";
    parentNod.style.display = "block";

    //Element for Search input field
    //Component Search-----------------------
    var search        = document.createElement("input");
    search.className  = "form-control";
    search.id         = "app_search";
    search.placeholder= "Search ...";
    search.name       = "search";
    search.type       = "text";

    var row =
      "" +
      '<div class = "row">' +
      '<div class="col col-12">' +
      "" +
      '<div class="col col-12">' +
      "</div> " +
      "</div>" +
      "</div>";
    var title = document.createElement("h1");
    title.textContent = "CVs";

    var card = "";
    
    //Contruct éléments to show
    user.announces.forEach((cv, index) => {
      card =
        '<div class="card mb-3 ' +
        "announce_" +
        index +
        '" style="max-width: 100%;">\n' +
        '<a href="#" class="app_card_a">' +
        '<div class="row no-gutters">\n' +
        '<div class="col-md-4" id="' +
        index +
        '" >\n' +
        '<img src="images/1.png" id="' +
        index +
        '"  class="card-img p-1" alt="exemple" style="height: 100%">\n' +
        "</div>\n" +
        '<div class="col-md-8" id="' +
        index +
        '" >\n' +
        '<div class="card-body app_card_p" id="' +
        index +
        '">\n' +
        '<div class="row" id="' +
        index +
        '">' +
        '<div class="text-danger app_cv_delete" id="' +
        index +
        '"><i class="fa fa-trash" ></i></div> ' +
        '<div class="col col-12"><h5>' +
        cv._companyName +
        "</h5></div>" +
          '<div class="col col-12 text-warning"><h5>'+ cv._job.toUpperCase() +'</h5></div>'+
        '<div class="col col-12"><p class="site" target="_blank"  href="' +
        cv._site +
        '">' +
        cv._site +
        "</p></div>" +
        '<div class="col col-12"><p>sent at ' +
        cv._date.toLocaleDateString() +
        "</p></div>" +
        "</div>" +
        "</div>\n" +
        "</div>\n" +
        "</div>" +
        "</a>" +
        "</div>" +
        card;
    });

    //create new Element
    //Component elementsList-------------------
    var div_home        = document.createElement("div");
    div_home.className  = "app_body_home";
    div_home.innerHTML  = row + card;

    //Button add CV
    //Component Boutton
    Form.button(".app_home", "Add", "btn btn-primary", "",()=> Store.app_add());
    parentNod.appendChild(search);
    parentNod.appendChild(div_home);

    //événement pour le titre => Home
    $(".titre_nav_user_name").addEventListener("click", Store.home);

    //Evenement pour le champ de recherche
    $("#app_search").addEventListener("keyup", Store.app_search);
    Store.listen(".app_card_a", "click", Store.app_show);
  }

  static listen(selector, onAction = "click", callback) {
    document.querySelectorAll(selector).forEach((card_a) => {
      card_a.addEventListener(onAction, callback);
    });
  }

  static raler(targetId, className, message) {
    var target = document.querySelector(targetId);
    var targetCopy = target.cloneNode(true);
    var div = document.createElement("div");
    div.className = className;
    div.innerHTML = "<strong>" + message + "</strong>.";
    if (
      target !== null &&
      target.firstChild !== null &&
      target.firstChild.classList.contains("alert") === false
    ) {
      target.innerHTML = "";
      target.appendChild(div);
      target.appendChild(targetCopy);
      setTimeout(() => {
        Store.app_home();
      }, 4000);
    }
  }

  static alert(targetId, className, message,m_sec = 1000) {
    var target      = document.querySelector(targetId);
    var div         = document.createElement("div");
    div.id          = "raler_alert";
    div.className   = className + " text-center col col-12 mt-5";
    div.innerHTML   = "<strong>" + message + "</strong>.";
    
    if (target !== null) {
        target.innerHTML = "";
        target.append(div);
        console.log(target)
        
      //on attend pour enlever le message d'alert affiché
      setTimeout(() => {
        Store.remove('#raler_alert');
      }, m_sec);
    }
  }

  static updateUser(user) {
    localStorage.setItem("user", JSON.stringify(user));
    user = Store.getUser("user");
  }

  /**
   * elle fait des recherches sur les éléments
   * présents affichés, selon la valeur à chercher, si
   * cette dernière match, les resultats seront affichés
   * et les autres seront marqués.
   * @param e
   */
  static app_search(e) {
    var inputValue  = e.target.value.toLowerCase();
    var itemsList   = document.querySelectorAll(".app_card_a");
    itemsList.forEach((item_a) => {
      if (item_a.innerText.toLowerCase().indexOf(inputValue) != -1) {
        //Show
        item_a.parentElement.style.display = "block";
      } else {
        //hide
        item_a.parentElement.style.display = "none";
      }
    });
  }

  //FOOTER FUNCTIONS
  static app_cv_update() {
    /**
     * Tableau ou liste de announces enregistrés
     * CVs envoyés dont la date d'envoi est inférieure à 2 semaines
     * @type {*[]}
     */
    let user      = User.userFromLocalStore('user');
    const getCVS  =  user => user.announces;
    //On filtre que les annonces qui respectent la conditions: _DAYS_NUMBER
    let announces = getCVS(User.userFromLocalStore("user"))
          .filter(cv => Store.calculeDiffDate(cv._date) < _DAYS_NUMBER);
    user.announces= announces;
    Store.app_home(user,[{ request: "date" }]);
  }

  static app_cv_deprecated() {
    /**
     * Tableau ou liste de announces enregistrés
     * @type {*[]}
     */
    let user  = User.userFromLocalStore('user');
    const getCVS =  user => user.announces;
    //On filtre que les annonces qui respectent la conditions: _DAYS_NUMBER
    let announces = getCVS(User.userFromLocalStore("user"))
        .filter(cv => Store.calculeDiffDate(cv._date) >= _DAYS_NUMBER);
    user.announces = announces;
    console.log("CVs envoyés dont la date d'envoi est supérieure ou égale à 2 semaines");
    Store.app_home(user,[{ request: "date" }]);
  }

  static app_cv_answered() {
    let user = User.userFromLocalStore('user');
    user.announces = user.announces.filter(announce => announce._answered === true);
    Store.app_home(user,[{ request: "date" }]);
    let elems = document.querySelectorAll('.app_card_p');
    let length = user.announces.length
    elems.forEach((elem,index) => {
      if(user.announces[length-1 - index]._state)
        elem.style.borderRight = "5px solid #00C851"
      else
        elem.style.borderRight = "5px solid #ff4444"
    })
    console.log("Status de toutes mes répondes");
  }

  static app_cv_rdv() {
    console.log("Mes rendez-vous");
  }

  static app_show_settings() {
    console.log("Afficher le menu");
  }
}
