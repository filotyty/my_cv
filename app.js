//VARIABLES

/**
 * Developpement
 */
const users = [
  {
    _announcesList: [
      {
        _companyName: "1998",
        _email: "duramana.kalumvuati@laposte.net",
        _site: "https://www.domain.extension",
        _date: "2020-02-01",
        _cp: 65755,
        _address: "17 Route de toute direction",
        _idAdvertissement: 7854,
        _state: false,
        _job: "DEveloppeur Web",
        _answered: false,
      },
      {
        _companyName: "Ecole d'Infoamtique de France",
        _email: "duramana.kalumvuati@laposte.net",
        _site: "https://www.domain.extension",
        _cp: 65755,
        _address: "17 Route de toute direction",
        _idAdvertissement: 7854,
        _date: "2020-07-10",
        _state: true,
        _job: "fgsdfg",
        _answered: true,
      },
      {
        _companyName: "cfii",
        _email: "filotyty@gmail.com",
        _site: "https://www.domain.extension",
        _cp: 65755,
        _address: "17 Route de toute direction",
        _idAdvertissement: 7854,
        _date: "2020-03-09",
        _state: false,
        _job: "fgsdfg",
        _answered: false,
      },
      {
        _companyName: "Centre de Formation Internation d'Infoirmatique",
        _email: "duramana.kalumvuati@laposte.net",
        _site: "https://www.domain.extension",
        _cp: 65755,
        _address: "17 Route de toute direction",
        _idAdvertissement: 7854,
        _date: "2020-07-2",
        _state: true,
        _job: "fgsdfg",
        _answered: true,
      },
      {
        _companyName: "CFII",
        _email: "filotyty@gmail.com",
        _site: "https://www.domain.extension",
        _cp: 65755,
        _address: "17 Route de toute direction",
        _idAdvertissement: 7854,
        _date: "2020-07-15",
        _state: false,
        _job: "Administateur Réseau",
        _answered: true,
      },{
        _companyName: "CFII",
        _email: "filotyty@gmail.com",
        _site: "https://www.domain.extension",
        _cp: 65755,
        _address: "17 Route de toute direction",
        _idAdvertissement: 7854,
        _date: "2020-07-15",
        _state: false,
        _job: "Administateur Réseau",
        _answered: true,
      },
    ],
    _name: "Duramana KALUMVUATI",
    _lastName: "Duramana",
    _email: "duramana.kalumvuati@laposte.net",
    _site: "https://www.domain.extension",
    _cv: "",
    _lm: "",
    _job: "Administrateur systeme",
    _answered: true,
  },
];

function addUser() {
  localStorage.setItem("user", JSON.stringify(users[0]));
}
/**
 * les <a> de cvc
 * @type {*[]}
 */
var app_cvs_click = [];

/**
 * variables pour les liens de footer
 * @type {*[]}
 */
var links = [];

/**
 * Utilisateur courant
 * @type {null}
 */

/**
 * Permet de faire comme jQuery: $('selector|id|class')
 * @param selector
 * @returns {any}
 */
const $ = (selector) => document.querySelector(selector);

//chargement de fenêtre
document.addEventListener("DOMContentLoaded", (e) => {

  e.preventDefault();
  //TODO - à supprimer lors de mise en place de Base de données

  //addUser();

  //Vérification de l'existance de l'utilisateur dans la mémoire de Browser
  //sinon, on lui propose de s'enregistrer = créer un compte
  let user = null;
  if (user = User.userFromLocalStore('user')) {
    //print HOME Page
    document.getElementById("app_home").innerHTML = user._name.toUpperCase();
    Store.app_home(user);

    //Addiction des evennement sut les bouttons de footer
    links = document.querySelectorAll("a");
    links.forEach((link) => {
      link.addEventListener("click", traiteClick);
    });
  } else {
    //Griser/cacher les bouttons - Footer, pour empecher de les cliquer

    User.signUp();

    const buttons = $(
      ".app_footer"
    ).firstElementChild.firstElementChild.querySelectorAll("div");

    buttons.forEach((div) => {
      //rendre
      div.firstElementChild.style.cursor = "not-allowed";
    });
  }
});

function saveFields(e) {
  e.preventDefault();
  //TODO à faire génériquement au lieu de faire en dure
  var name      = $("#name").value;
  var lastname  = $("#lastname").value;
  var email     = $("#email").value;
  var cv        = $("#cv").value;
  var lm        = $("#lm").value;
  var newUser   = null;

  //TODO - A completer ou finir
  var files = document.querySelector("#cv").files;
  //Form.updateFile('#cv')

  //---------------------------------------------------------
  if (name.length === 0 || lastname.length === 0 || email.length === 0) {
    Store.alert(".app_body","alert alert-danger ","Vérifiez les champs obligatoires",3000)
    $("#add").addEventListener("click",saveFields)
  } else {
    newUser = new User(name, lastname, email);
    newUser.cv = cv;
    newUser.lm = lm;

    localStorage.setItem("user", JSON.stringify(newUser));
    Store.cleanField("input");
    //update userName
    document.getElementById("app_home").innerHTML = name.toUpperCase();

    //Passer vers les vcs après avoir masqué le Home - page
    $(".app_signup").remove();
    Store.app_home(User.userFromLocalStore('user'));

    const buttons = $(
        ".app_footer"
    ).firstElementChild.firstElementChild.querySelectorAll("div");

    buttons.forEach((div) => {
      //rendre
      div.firstElementChild.style.cursor = "pointer";
      div.firstElementChild.addEventListener('click',traiteClick);
    });
  }
}

/**
 * Evenement lors qu'on clique sur <a>
 * @param E
 */
function traiteClick(E) {
  E.preventDefault();
  switch (E.currentTarget.id) {
    case "app_cv_update":
      Store.app_cv_update();
      break;
    case "app_cv_deprecated":
      Store.app_cv_deprecated();
      break;
    case "app_cv_answered":
      Store.app_cv_answered();
      break;
    case "app_cv_rdv":
      Store.app_cv_rdv();
      break;
    case "app_show_settings":
      Store.app_show_settings();
      break;
    default:
  }
}
