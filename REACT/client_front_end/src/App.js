import React, {useContext} from 'react';
import './App.css';
import NavBar from "./navbar/NavBar";
import Footer from "./navbar/Footer";
import Home from "./components/Home";
import {BrowserRouter as Router,Route, Switch} from "react-router-dom";
import Login from "./admin/Login";
import {GlobalProvider} from './conts/GlobalState'
import {AnnounceForm} from "./components/AnnounceForm";

function App() {
  return (
    <GlobalProvider >

        {/* GEstion de Routes */}
        <Router >
            <Switch>
                <Route path="/login" component={Login}></Route>
                <Route path="/add" component={AnnounceForm}></Route>
                <Route path="/" component={Home}></Route>
            </Switch>
        </Router>
        <Footer/>
    </GlobalProvider>
  );
}

export default App;
