import React, {Component} from "react";
import FormInfos from './FormInfos'
import {ContactForm} from "./ContactForm";
import {AddressForm} from "./AddressForm";
import {AnswerForm} from "./AnswerForm";
import {Confirm} from "./Confirm";
import {Success} from "./Success";
import Home from "./Home";

export class AnnounceForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            step:5,
            _companyName: "Banque Centrale de France",
            _job: "Administrateur Réseaux",
            _idAdvertissement: 54651,
            _email: "infos@tft.fr",
            _site: "www.tutofree.fr",
            _telephone: +33753800611,
            _date: Date.now(),
            _cp: 76000,
            _address:'15 Route de Tous',
            _city: 'Manzela',
            _answered: true,
            _answeredStatus:false,
            _usefulInfo:"Démande sur l'évolution du poste", //Information importantes sur l'entreprise
            _questions:['Comment vous faites pour manger?','Dequelle origine est cette entreprise?'], //listes de questions sur l'entreprise lors de l'entretien
            _interviewDate: new Date() // date de l'entretien
        }
        this.nextStep   = this.nextStep.bind(this)
        this.prevStep   = this.prevStep.bind(this)
        this.onChanged  = this.onChanged.bind(this)
        this.confirme   = this.confirme.bind(this)
    }
    nextStep    = ()=> {
        this.setState({
            step: this.state.step + 1
        })
    };

    prevStep    = ()=> {
        this.setState({ step: this.state.step - 1 })
    };

    onChanged   = input =>e=>{
        this.setState({
            [input]:e.target.value
        })
    }

    //Pour les checkBoxs
    onChecked   = input =>e=>{
        this.setState({
            [input]:e.target.checked
        })
    }

    //TODO - additionner les informations
    //sur une base de donnée
    confirme = (e)=> {
        console.log("confirmation de données");
    }

    render() {
        const {step} = this.state;

        const {
            _companyName,
            _job,
            _idAdvertissement,
            _email,
            _site,
            _telephone,
            _date,
            _cp,
            _city,
            _answered,
            _answeredStatus,
            _address,
            _usefulInfo,
            _questions,
            _interviewDate } = this.state;

        const values = {
            _companyName,
            _job,
            _idAdvertissement,
            _email,
            _site,
            _telephone,
            _date,
            _cp,
            _city,
            _answered,
            _answeredStatus,
            _address,
            _usefulInfo,
            _questions,
            _interviewDate};

        switch (step ) {
            case 1:
                return (

                    <FormInfos
                        values      = {values}
                        nextStep    = {this.nextStep}
                        onChanged   = {this.onChanged}
                    />
                );
            case 2:
                return (
                    <ContactForm
                        values    = {values}
                        nextStep  = {this.nextStep}
                        prevStep  = {this.prevStep}
                        onChanged = {this.onChanged}
                    />
                );
            case 3:
                return (<AddressForm
                    values    = {values}
                    onConfirme= {this.confirme}
                    nextStep  = {this.nextStep}
                    prevStep  = {this.prevStep}
                    onChanged = {this.onChanged}

                />)
            case 4:
                return (<AnswerForm
                    values    = {values}
                    onConfirme= {this.confirme}
                    nextStep  = {this.nextStep}
                    prevStep  = {this.prevStep}
                    onChecked = {this.onChecked}
                    onChanged = {this.onChanged}


                />)
            case 5:
                return (<Confirm
                    values    = {values}
                    onConfirme= {this.confirme}
                    nextStep  = {this.nextStep}
                    prevStep  = {this.prevStep}
                />)
            case 6:
                return <Success/>
            default:
                return <Home/>
        }
    }
}

export default AnnounceForm;
